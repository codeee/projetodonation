using Microsoft.EntityFrameworkCore;

namespace ProjetoDonation.Models
{
  public class ApiDbContext : DbContext
  {
    public ApiDbContext(DbContextOptions<ApiDbContext> options)
        : base(options)
    { }

    public DbSet<Tarefa> Tarefas { get; set; }
    public DbSet<Execucao> Execucoes { get; set; }
    public DbSet<Arquivo> Arquivos { get; set; }
  }
}
