using System;

namespace ProjetoDonation.Models
{
    public class Produto
    {
        private string Nome{ get; set; }
        private string Tipo { get; set; }
        private string Descricao { get; set; }
        public Doador doador {get; set;}
        public Cliente clientes {get; set;}
    }

}