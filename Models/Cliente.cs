using System;
using System.Collections.Generic;

namespace ProjetoDonation.Models
{
    public class Cliente
    {
        private string Nome { get; set; }
        private int Cpf { get; set; }
        private string Telefone { get; set; }
        public ICollection<Produto> produtos { get; set; }
    }

}