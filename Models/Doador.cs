using System.Collections.Generic;

namespace ProjetoDonation.Models
{
    public class Doador
    {
        private string Nome { get; set; }
        private int Cpf { get; set; }
        private string Telefone { get; set; }
        public ICollection<Produto> produtos { get; set; }
        public ICollection<Cliente> clientes { get; set; }
    }

}